import { Component } from '@angular/core';
import { WeatherService } from './weather.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Weather';
  detailedWeather;
  cityList

  constructor(
    private _weatherService: WeatherService,
  ) {
    this.cityList = _weatherService.getService();
  }

  citySelected(city) {
    this.detailedWeather = this._weatherService.getSpecificCity(city);
  }
}

