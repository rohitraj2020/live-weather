import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {City} from './types';
import {Observable} from 'rxjs';

const URL = 'http://api.openweathermap.org/data/2.5/'

const requestConst = {
  units: 'metric',
  appid: '5fd23bf7f8865ca88a9f096cf0fe779f',
};

@Injectable({
  providedIn: 'root'
})

export class WeatherService {

  constructor(private _httpClient: HttpClient) {}

  getService(): Observable < City > {
    const API = URL + 'group?';
    const options = {
      params: new HttpParams({
        fromObject: {
          id: '3117735,703448,2643743,6453366,6455259',
          ...requestConst
        }
      })
    };
    return this._httpClient.get < City > (API, options);
  }


  getSpecificCity(id): Observable < City > {
    const API = URL + 'forecast?id=' + id;
    const options = {
      params: new HttpParams({
        fromObject: {
          ...requestConst
        }
      })
    };
    return this._httpClient.get < City > (API, options);
  }


}
